import React, { Component } from 'react';
import logo from '../images/logo.png'
import '../scss/footer.scss'

class Footer extends Component {
  render() {
    return (
      <>
      <div className="footer">
      <img src={logo} alt='logo'/>
      <div className="grey-line"></div>
      <div className="icon-footer">
      <i className="fab fa-facebook"></i>
      <i className="fab fa-google-plus-square"></i>
      <i className="fab fa-twitter-square"></i>
      <i className="fab fa-linkedin"></i>
      <i className="fab fa-dribbble"></i>
      <i className="fab fa-pinterest-square"></i>
      <i className="fab fa-instagram"></i>
      </div>
      <div className="grey-line"></div>
      <p>© 2016 Mulitix Theme by ThemeForces. All Rights Reserved.</p>
      </div>
      </>
    );
  }
}

export default Footer;