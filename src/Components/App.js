import React, { Component } from 'react';
import '../scss/App.scss';
import Grid from './grid/Grid';
import Header from './Header'
import Footer from './Footer';
import CarouselV1 from './carousel/Carousel';

class App extends Component {
  render() {
    return (
      <div className="App">
        <Header/>
        <CarouselV1/>
        <Grid/>
        <Footer/>
      </div>
    );
  }
}

export default App;
