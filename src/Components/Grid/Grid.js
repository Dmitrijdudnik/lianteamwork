import React, { Component } from 'react';
import GridNav from './GridNav';
import GridImg from './GridImg';
import GridBtn from './GridBtn';

class Grid extends Component {
  render() {
    return (
      <section className='grid'>
        <GridNav/>
        <GridImg/>
        <GridBtn/>
      </section>
    );
  }
}

export default Grid;
