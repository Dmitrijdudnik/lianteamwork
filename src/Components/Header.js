import React, { Component } from 'react';
import logo from '../images/logo.png'
import '../scss/header.scss'

class Header extends Component {
  render() {
    return (
      <>
      <div className="header">
      <img src={logo} alt='logo'/>
      <ul className='menu-list'>
<li><i className="fas fa-minus"></i></li>
<li>HOME</li>
<li>About</li>
<li>Portfolio</li>
<li>BLOG</li>
<li>Contact</li>
<li><i className="fas fa-search"></i></li>
      </ul>
      </div>
      </>
    );
  }
}

export default Header;
